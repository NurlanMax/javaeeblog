package kz.javaeesession.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DBManager {
    private static Connection connection;

    static {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection
                    ("jdbc:postgresql://localhost:5432/postgres", "postgres", "megatiger1998");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static AuthUser getUser(String email) {
        AuthUser user = null;
        try {
            PreparedStatement statement = connection.prepareStatement("" +
                    "SELECT * FROM users WHERE email = ?");
            statement.setString(1, email);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                user = new AuthUser(
                        resultSet.getLong("id"),
                        resultSet.getString("email"),
                        resultSet.getString("password"),
                        resultSet.getString("full_name")
                );
            }
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    public static boolean saveUser(AuthUser user) {

        int rows = 0;
        try {
            PreparedStatement statement = connection.prepareStatement("" +
                    "UPDATE users SET full_name=? WHERE id=?");
            statement.setString(1, user.getFullName());
            statement.setLong(2, user.getId());
            rows = statement.executeUpdate();
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0;
    }

    public static boolean savePassword(AuthUser user) {
        int rows = 0;
        boolean exists = false;
        try {
            PreparedStatement statement = connection.prepareStatement("" +
                    "SELECT * FROM password_histories WHERE user_id=? AND password=?");
            statement.setLong(1, user.getId());
            statement.setString(2, user.getPassword());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                exists = true;
            }
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!exists) {
            try {
                PreparedStatement statement = connection.prepareStatement("" +
                        "UPDATE users SET password=? WHERE id=?");
                statement.setString(1, user.getPassword());
                statement.setLong(2, user.getId());
                rows = statement.executeUpdate();
                statement.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                PreparedStatement statement = connection.prepareStatement("" +
                        "INSERT INTO password_histories (user_id, password, added_date) " +
                        "VALUES (?, ?, NOW())");
                statement.setLong(1, user.getId());
                statement.setString(2, user.getPassword());
                statement.executeUpdate();
                statement.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return rows > 0;
    }

    public static boolean addUser(AuthUser user) {
        int rows = 0;

        Long lastInsertUserId = null;

        try {
            PreparedStatement statement = connection.prepareStatement("" +
                    "INSERT INTO users (email, password, full_name) " +
                    "VALUES (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);

            statement.setString(1, user.getEmail());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getFullName());

            rows = statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                lastInsertUserId = rs.getLong(1);
            }

            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (lastInsertUserId != null) {
            try {
                PreparedStatement statement = connection.prepareStatement("" +
                        "INSERT INTO password_histories (user_id, password, added_date) " +
                        "VALUES (?,?,NOW())");

                statement.setLong(1, lastInsertUserId);
                statement.setString(2, user.getPassword());
                statement.executeUpdate();
                statement.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return rows > 0;
    }

    public static boolean addBlog(Blog blog) {
        int rows = 0;

        try {

            PreparedStatement statement = connection.prepareStatement("" +
                    "INSERT INTO blogs (user_id, title, short_content, content, post_date) " +
                    "VALUES (?,?,?,?, NOW())");
            statement.setLong(1, blog.getUser().getId());
            statement.setString(2, blog.getTitle());
            statement.setString(3, blog.getShortContent());
            statement.setString(4, blog.getContent());

            rows = statement.executeUpdate();
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0;
    }

    public static Blog getBlog(Long id) {
        Blog blog = null;

        try {
            PreparedStatement statement = connection.prepareStatement("" +
                    "SELECT b.id, b.title, b.content, b.short_content, b.user_id, b.post_date, u.full_name, b.like_amount " +
                    "FROM blogs b " +
                    "INNER JOIN users u on b.user_id=u.id " +
                    "WHERE b.id=? ");
            statement.setLong(1, id);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                blog = new Blog(rs.getLong("id"),
                        new AuthUser(
                                rs.getLong("user_id"),
                                null, null,
                                rs.getString("full_name")
                        ),
                        rs.getString("title"),
                        rs.getString("short_content"),
                        rs.getString("content"),
                        rs.getTimestamp("post_date"),
                        rs.getInt("like_amount")
                );
                statement.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return blog;
    }

    public static ArrayList<Blog> getAllBlogs() {
        ArrayList<Blog> blogs = new ArrayList<>();

        try {
            PreparedStatement statement = connection.prepareStatement("" +
                    "SELECT b.id, b.title, b.content, b.short_content, b.user_id, b.post_date, u.full_name, b.like_amount " +
                    "FROM blogs b " +
                    "INNER JOIN users u on b.user_id=u.id " +
                    "ORDER BY b.post_date DESC ");

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                blogs.add(
                        new Blog(rs.getLong("id"),
                                new AuthUser(
                                        rs.getLong("user_id"),
                                        null, null,
                                        rs.getString("full_name")
                                ),
                                rs.getString("title"),
                                rs.getString("short_content"),
                                rs.getString("content"),
                                rs.getTimestamp("post_date"),
                                rs.getInt("like_amount")
                        )
                );
                statement.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return blogs;
    }

    public static boolean saveBlog(Blog blog) {
        int rows = 0;

        try {

            PreparedStatement statement = connection.prepareStatement("" +
                    "UPDATE blogs SET title=?, short_content=?, content=? " +
                    "WHERE id=?");


            statement.setString(1, blog.getTitle());
            statement.setString(2, blog.getShortContent());
            statement.setString(3, blog.getContent());
            statement.setLong(4, blog.getId());


            rows = statement.executeUpdate();
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0;
    }

    public static boolean deleteBlog(Blog blog) {
        int rows = 0;

        try {

            PreparedStatement statement = connection.prepareStatement("" +
                    "DELETE FROM comments WHERE blog_id= ?");


            statement.setLong(1, blog.getId());

            statement.executeUpdate();
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            PreparedStatement statement = connection.prepareStatement("" +
                    "DELETE FROM blogs WHERE id=?");


            statement.setLong(1, blog.getId());

            rows = statement.executeUpdate();
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0;
    }

    public static boolean addComment(Comment comment) {
        int rows = 0;

        try {

            PreparedStatement statement = connection.prepareStatement("" +
                    "INSERT INTO comments (user_id, blog_id, comment, post_date) " +
                    "VALUES (?,?,?,NOW())");
            statement.setLong(1, comment.getUser().getId());
            statement.setLong(2, comment.getBlog().getId());
            statement.setString(3, comment.getComment());

            rows = statement.executeUpdate();
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows > 0;
    }

    public static ArrayList<Comment> getCommentsById(Long blogId) {
        ArrayList<Comment> comments = new ArrayList<>();

        try {
            PreparedStatement statement = connection.prepareStatement("" +
                    "SELECT c.id, c.post_date, c.comment, c.user_id, c.blog_id, u.full_name " +
                    "FROM comments c " +
                    "INNER JOIN users u on c.user_id=u.id " +
                    "WHERE c.blog_id=? " +
                    "ORDER BY c.post_date DESC ");
            statement.setLong(1, blogId);

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                comments.add(
                        new Comment(
                                rs.getLong("id"),
                                new AuthUser(
                                        rs.getLong("user_id"),
                                        null, null,
                                        rs.getString("full_name")
                                ),
                                new Blog(rs.getLong("blog_id")),
                                rs.getString("comment"),
                                rs.getTimestamp("post_date")
                        )
                );

                statement.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return comments;
    }

    public static HashMap<String, String> likeBlog(Blog blog, AuthUser user) {
        int likes = 0;
        boolean liked = false;
        liked=checkedLike(blog,user);
        try {
            PreparedStatement statement;
            if (liked) {
                statement = connection.prepareStatement("DELETE FROM likes WHERE blog_id =? AND user_id=? ");
            } else {
                statement = connection.prepareStatement("INSERT INTO likes (blog_id, user_id) VALUES (?,?)");
            }
            statement.setLong(1, blog.getId());
            statement.setLong(2, user.getId());

            statement.executeUpdate();
            statement.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            PreparedStatement statement=connection.prepareStatement("" +
                    "SELECT COUNT(*) AS like_amount FROM likes WHERE blog_id = ? ");
            statement.setLong(1,blog.getId());

            ResultSet rs=statement.executeQuery();
            if(rs.next()) {
                likes=rs.getInt("like_amount");
            }
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PreparedStatement statement=connection.prepareStatement("" +
                    "UPDATE blogs SET like_amount=? WHERE id= ?");
            statement.setInt(1,likes);
            statement.setLong(2,blog.getId());
            statement.executeUpdate();
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        HashMap<String, String> map=new HashMap<>();
        map.put("liked", !liked+"");
        map.put("likes", likes+"");
        return map;
    }
    public static boolean checkedLike(Blog blog, AuthUser user) {
        boolean liked=false;
        try {
            PreparedStatement statement = connection.prepareStatement("" +
                    "SELECT * FROM likes WHERE blog_id= ? AND user_id= ? ");
            statement.setLong(1, blog.getId());
            statement.setLong(2, user.getId());
            ResultSet rs = statement.executeQuery();

            if (rs.next()) {
                liked = true;
            }
            statement.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return liked;

    }
}
