package kz.javaeesession.db;

import java.sql.Date;
import java.sql.Timestamp;

public class Blog {
    private Long id;
    private AuthUser user;
    private String title;
    private String shortContent;
    private String content;
    private Timestamp postDate;
    private int likeAmount;

    public Blog(Long id, AuthUser user, String title, String shortContent, String content, Timestamp postDate) {
        this.id = id;
        this.user = user;
        this.title = title;
        this.shortContent = shortContent;
        this.content = content;
        this.postDate = postDate;
    }

    public Blog(Long id, AuthUser user, String title, String shortContent, String content, Timestamp postDate, int likeAmount) {
        this.id = id;
        this.user = user;
        this.title = title;
        this.shortContent = shortContent;
        this.content = content;
        this.postDate = postDate;
        this.likeAmount = likeAmount;
    }

    public Blog() {
    }
    public Blog(Long id) {
        this.id=id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AuthUser getUser() {
        return user;
    }

    public void setUser(AuthUser user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortContent() {
        return shortContent;
    }

    public void setShortContent(String shortContent) {
        this.shortContent = shortContent;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getPostDate() {
        return postDate;
    }

    public void setPostDate(Timestamp postDate) {
        this.postDate = postDate;
    }

    public int getLikeAmount() {
        return likeAmount;
    }

    public void setLikeAmount(int likeAmount) {
        this.likeAmount = likeAmount;
    }
}
