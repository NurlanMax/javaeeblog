package kz.javaeesession.db;

import java.sql.Timestamp;

public class Comment {
    private Long id;
    private AuthUser user;
    private Blog blog;
    private String comment;
    private Timestamp postdate;

    public Comment(Long id, AuthUser user, Blog blog, String comment, Timestamp postdate) {
        this.id = id;
        this.user = user;
        this.blog = blog;
        this.comment = comment;
        this.postdate = postdate;
    }

    public Comment() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AuthUser getUser() {
        return user;
    }

    public void setUser(AuthUser user) {
        this.user = user;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Timestamp getPostdate() {
        return postdate;
    }

    public void setPostdate(Timestamp postdate) {
        this.postdate = postdate;
    }
}
