package kz.javaeesession.servlets;

import kz.javaeesession.db.AuthUser;
import kz.javaeesession.db.Blog;
import kz.javaeesession.db.DBManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/editblog")
public class EditBlogServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String redirect="/login";
        request.setCharacterEncoding("UTF8");
        AuthUser authUser = (AuthUser)request.getSession().getAttribute("CURRENT_USER");
        if (authUser != null) {
            redirect="/";

            Long blogId=0L;
            try {
                blogId=Long.parseLong(request.getParameter("blog_id"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Blog blog= DBManager.getBlog(blogId);

            if(blog!=null&& blog.getUser().getId()==authUser.getId()) {

                String title=request.getParameter("title");
                String shortContent=request.getParameter("short_content");
                String content=request.getParameter("content");

                blog.setTitle(title);
                blog.setContent(content);
                blog.setShortContent(shortContent);

                if(DBManager.saveBlog(blog)) {
                    redirect="/editblog?id="+blog.getId()+"&success";
                }
            }



        }
        response.sendRedirect(redirect);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AuthUser authUser=(AuthUser)request.getSession().getAttribute("CURRENT_USER");
        if(authUser!=null) {
            Long id=0L;
            try {
                id=Long.parseLong(request.getParameter("id"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Blog blog= DBManager.getBlog(id);
            if(blog!=null && blog.getUser().getId()==authUser.getId()) {
                request.setAttribute("blog", blog);
                request.getRequestDispatcher("/editblog.jsp").forward(request, response);
            } else {
                response.sendRedirect("/");
            }
        } else {
            response.sendRedirect("");
        }
    }
}
