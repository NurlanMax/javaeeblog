package kz.javaeesession.servlets;

import kz.javaeesession.db.AuthUser;
import kz.javaeesession.db.DBManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(value = "/register")
public class RegisterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String redirect="/register?error";
        String email=request.getParameter("email");
        String fullName=request.getParameter("full_name");
        String password=request.getParameter("password");
        String rePassword=request.getParameter("re_password");
        AuthUser user= DBManager.getUser(email);

        if(user==null) {
            redirect="/register?passworderror";
            if(rePassword.equals(password)) {
                AuthUser newUser=new AuthUser(null, email,password,fullName);
                DBManager.addUser(newUser);
                redirect="/register?success";
            }
        }
        response.sendRedirect(redirect);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/register.jsp").forward(request, response);
    }
}
