package kz.javaeesession.servlets;

import kz.javaeesession.db.Blog;
import kz.javaeesession.db.DBManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(value = "/blog")
public class BlogServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ArrayList<Blog> blogs= DBManager.getAllBlogs();
        request.setAttribute("blogs", blogs);
        request.getRequestDispatcher("/blog.jsp").forward(request, response);
    }
}
