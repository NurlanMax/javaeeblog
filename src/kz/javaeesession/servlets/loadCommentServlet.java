package kz.javaeesession.servlets;

import com.google.gson.Gson;
import kz.javaeesession.db.AuthUser;
import kz.javaeesession.db.Blog;
import kz.javaeesession.db.Comment;
import kz.javaeesession.db.DBManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet(value = "/loadcomments")
public class loadCommentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String result = "ERROR";
        request.setCharacterEncoding("UTF8");
        AuthUser authUser = (AuthUser) request.getSession().getAttribute("CURRENT_USER");
        if (authUser != null) {


            Long blogId = 0L;
            try {
                blogId = Long.parseLong(request.getParameter("blog_id"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Blog blog = DBManager.getBlog(blogId);

            if (blog!= null) {
                String commentText=request.getParameter("comment");

                Comment comment=new Comment(null, authUser,blog, commentText,null);
                if(DBManager.addComment(comment)) {

                    result="OK";
                }
            }
        }
        PrintWriter out=response.getWriter();
        out.print(result);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long id=0L;
        try {
            id=Long.parseLong(request.getParameter("blog_id"));
        } catch (Exception e){
            e.printStackTrace();
        }
        ArrayList<Comment> comments= DBManager.getCommentsById(id);
        Gson gson=new Gson();
        String jsonText=gson.toJson(comments);
        PrintWriter out=response.getWriter();
        out.println(jsonText);
    }
}
