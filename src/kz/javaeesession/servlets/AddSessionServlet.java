package kz.javaeesession.servlets;

import kz.javaeesession.db.SessionUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(value = "/addsession")
public class AddSessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name=request.getParameter("name");
        String surname=request.getParameter("surname");
        String age=request.getParameter("age");

        int finalAge=0;
        try{
            finalAge=Integer.parseInt(age);
        } catch (Exception e) {
            e.printStackTrace();
        }
        SessionUser user=new SessionUser(name,surname,finalAge);
        HttpSession session=request.getSession();
        session.setAttribute("USER_DATA",user);
        response.sendRedirect("session");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
