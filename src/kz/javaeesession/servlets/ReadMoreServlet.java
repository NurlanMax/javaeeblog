package kz.javaeesession.servlets;

import kz.javaeesession.db.AuthUser;
import kz.javaeesession.db.Blog;
import kz.javaeesession.db.Comment;
import kz.javaeesession.db.DBManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(value = "/readmore")
public class ReadMoreServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AuthUser authUser=(AuthUser)request.getSession().getAttribute("CURRENT_USER");

        Long id=0L;
        try {
            id=Long.parseLong(request.getParameter("id"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Blog blog=DBManager.getBlog(id);

        if(blog!=null){
            request.setAttribute("blog", blog);
            ArrayList<Comment> comments=DBManager.getCommentsById(blog.getId());
            boolean liked=false;
            if(authUser!=null){
                liked=DBManager.checkedLike(blog,authUser);
            }
            request.setAttribute("liked", liked);
            request.setAttribute("comments", comments);
        }

        request.getRequestDispatcher("/readmore.jsp").forward(request, response);
    }
}
