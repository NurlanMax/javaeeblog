package kz.javaeesession.servlets;

import kz.javaeesession.db.AuthUser;
import kz.javaeesession.db.Blog;
import kz.javaeesession.db.DBManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/addblog")
public class AddBlogServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String redirect="/login";
        request.setCharacterEncoding("UTF8");
        AuthUser authUser = (AuthUser)request.getSession().getAttribute("CURRENT_USER");
        if (authUser != null) {
            redirect="/";

            String title=request.getParameter("title");
            String shortContent=request.getParameter("short_content");
            String content=request.getParameter("content");

            Blog blog=new Blog(null,authUser,title,shortContent,content,null);
            if(DBManager.addBlog(blog)) {
                redirect="/addblog?success";
            }

        }
        response.sendRedirect(redirect);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AuthUser authUser = (AuthUser) request.getSession().getAttribute("CURRENT_USER");
        if (authUser != null) {
            request.getRequestDispatcher("/addblog.jsp").forward(request, response);
        } else {
            response.sendRedirect("/login");
        }
    }
}
