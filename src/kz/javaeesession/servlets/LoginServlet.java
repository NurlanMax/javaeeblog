package kz.javaeesession.servlets;

import kz.javaeesession.db.AuthUser;
import kz.javaeesession.db.DBManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(value="/login")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String redirect="/login?error";
        String email=request.getParameter("email");
        String password=request.getParameter("password");
        AuthUser user= DBManager.getUser(email);
        if(user!=null){
            if(user.getPassword().equals(password)){
                HttpSession session=request.getSession();
                session.setAttribute("CURRENT_USER",user);
                redirect="/profile";
            }
        }
        response.sendRedirect(redirect);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/login.jsp").forward(request,response);
    }
}
