<%--
  Created by IntelliJ IDEA.
  User: Nurik
  Date: 03/02/2021
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({selector:'textarea'});</script>
</head>
<body>
<%@include file="navbar.jsp"%>
<div class="container">
    <div class="row mt-5">
        <div class="col-10 mx-auto">
            <%
                String success=request.getParameter("success");
                if(success!=null){
            %>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Blog added successfully!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <%
                }
            %>
            <form action="/addblog" method="post">
                <div class="form-group">
                    <label>TITLE:</label>
                    <input type="text" class="form-control" name="title">
                </div>
                <div class="form-group">
                    <label>SHORT CONTENT:</label>
                    <textarea type="text" class="form-control" name="short_content" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <label>CONTENT:</label>
                    <textarea type="text" class="form-control" name="content" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-success">ADD BLOG</button>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</html>
