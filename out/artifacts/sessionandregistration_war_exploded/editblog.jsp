<%@ page import="kz.javaeesession.db.Blog" %><%--
  Created by IntelliJ IDEA.
  User: Nurik
  Date: 03/02/2021
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({selector:'textarea'});</script>
</head>
<body>
<%@include file="navbar.jsp"%>
<div class="container">
    <div class="row mt-5">
        <div class="col-10 mx-auto">
            <%
                String success=request.getParameter("success");
                if(success!=null){
            %>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Blog saved successfully!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <%
                }
            %>
            <%
                Blog blog=(Blog)request.getAttribute("blog");
                if(blog!=null) {
            %>
            <form action="/editblog" method="post">
                <input type="hidden" name="blog_id" value="<%=blog.getId()%>">
                <div class="form-group">
                    <label>TITLE:</label>
                    <input type="text" class="form-control" name="title" value="<%=blog.getTitle()%>">
                </div>
                <div class="form-group">
                    <label>SHORT CONTENT:</label>
                    <textarea type="text" class="form-control" name="short_content" rows="5"><%=blog.getShortContent()%></textarea>
                </div>
                <div class="form-group">
                    <label>CONTENT:</label>
                    <textarea type="text" class="form-control" name="content" rows="10"><%=blog.getContent()%></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-success">SAVE BLOG</button>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteBlog">
                        DELETE BLOG
                    </button>
                </div>
            </form>

            <div class="modal fade" id="deleteBlog" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="deleteblog" method="post">
                            <input type="hidden" name="blog_id" value="<%=blog.getId()%>">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Delete Blog</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Are you sure?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                            <button type="submit" class="btn btn-danger">Yes</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <%
                } else {
            %>
            <h1 class="text-center">
                404 BLOG NOT FOUND
            </h1>
            <%
                }
            %>
        </div>
    </div>
</div>

</body>
<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</html>
