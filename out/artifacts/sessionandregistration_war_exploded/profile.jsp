<%--
  Created by IntelliJ IDEA.
  User: Nurik
  Date: 01/02/2021
  Time: 14:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<%@include file="navbar.jsp"%>
<div class="container">
    <div class="row mt-5">
        <div class="col-6 mx-auto">
            <%
                if(currentUser!=null) {
            %>
            <%
                String success=request.getParameter("success");
                if(success!=null){
            %>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                User updated successfully!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <%
                }
            %>
                <form action="/updateprofile" method="post">
                    <div class="form-group">
                        <label>
                            EMAIL:
                        </label>
                        <input type="email" value="<%=currentUser.getEmail()%>" class="form-control" readonly>
                    </div>
                    <div class="form-group">
                        <label>
                            FULL NAME:
                        </label>
                        <input type="text" value="<%=currentUser.getFullName()%>" class="form-control" name="full_name">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success">
                            UPDATE PROFILE DATA
                        </button>
                    </div>
                </form>
            <%
                }
            %>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-6 mx-auto">
            <%
                if(currentUser!=null) {
            %>
            <%
                String passwordSuccess=request.getParameter("passwordsuccess");
                if(passwordSuccess!=null){
            %>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Password updated successfully!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <%
                }
            %>
            <%
                String passwordError=request.getParameter("passworderror");
                if(passwordError!=null){
            %>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                Couldn't update password!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <%
                }
            %>
            <%
                String passwordRepeat=request.getParameter("passwordrepeat");
                if(passwordRepeat!=null){
            %>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                Your password is used before!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <%
                }
            %>
            <form action="/updatepassword" method="post">
                <div class="form-group">
                    <label>
                        OLD PASSWORD :
                    </label>
                    <input type="password" class="form-control" name="old_password">
                </div>
                <div class="form-group">
                    <label>
                        NEW PASSWORD :
                    </label>
                    <input type="password" class="form-control" name="new_password">
                </div>
                <div class="form-group">
                    <label>
                        RETYPE NEW PASSWORD :
                    </label>
                    <input type="password" class="form-control" name="re_new_password">
                </div>
                <div class="form-group">
                    <button class="btn btn-success">
                      UPDATE PASSWORD
                    </button>
                </div>
            </form>
            <%
                }
            %>
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</html>
