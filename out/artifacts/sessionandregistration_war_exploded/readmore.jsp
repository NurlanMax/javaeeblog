<%@ page import="java.util.ArrayList" %>
<%@ page import="kz.javaeesession.db.Blog" %>
<%@ page import="kz.javaeesession.db.Comment" %><%--
  Created by IntelliJ IDEA.
  User: Nurik
  Date: 03/02/2021
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body onload="loadComments()">
<%@include file="navbar.jsp"%>
<div class="container pb-5">
    <div class="row mt-5">
        <div class="col-12">
            <%
                Blog blog=(Blog)request.getAttribute("blog");
                if(blog!=null) {
            %>
            <div class="jumbotron">
                <h1>
                    <%=blog.getTitle()%>
                </h1>
                <p class="lead">
                    <%=blog.getShortContent()%>
                </p>
                <p class="lead">
                    <%=blog.getContent()%>
                </p>
                <hr class="my-4">
                <p>
                    Posted by <b><%=blog.getUser().getFullName()%></b> at <b><%=blog.getPostDate()%></b>
                </p>
                <p>
                    <table cellpadding="5px">
                        <tr>
                            <td>
                                <%
                                    String color="color:black;";
                                    boolean liked=(boolean)request.getAttribute("liked");
                                    if(liked){
                                        color="color:red;";
                                    }
                                %>
                                <a id="heart" href="JavaScript:void(0)" style="<%=color%>font-size: 30px; text-decoration: none;" <% if(currentUser!=null) {%>onclick="toLike()"<%}%>>&hearts;</a>
                            </td>
                            <td>
                                <span id="like_amount"><%=blog.getLikeAmount()%></span>
                                <span>likes</span>
                            </td>
                        </tr>
                    </table>
                </p>
                <%
                    if(currentUser!=null){
                %>
                    <script type="text/javascript">
                        function toLike() {
                            $.post("/tolike",
                                    {
                                        blog_id: <%=blog.getId()%>
                                    }, function (result) {
                                if(result!="ERROR") {
                                    // alert(result);
                                    // console.log(result);
                                    var likeStat=JSON.parse(result);
                                    $("#like_amount").html(likeStat['likes']);
                                    if(likeStat['is_liked']==true) {
                                        $("#heart").css("color","red");
                                    } else {
                                        $("#heart").css("color","black");
                                    }
                                    //document.getElementById("like_amount").innerHTML=result;
                                }
                                }
                                );
                        }
                    </script>
                <%
                    }
                %>
                <p>
                    <%
                        if(currentUser!=null&&currentUser.getId()==blog.getUser().getId()){
                    %>
                    <a href="/editblog?id=<%=blog.getId()%>" class="btn btn-primary btn-sm">EDIT BLOG</a>
                    <%
                        }
                    %>
                </p>
            </div>

                <h3>Comments:</h3>
            <%
                if(currentUser!=null) {
            %>
                <div class="row mt-3" id="commentDiv">
                    <div class="col-12">

                            <input type="hidden" name="blog_id" value="<%=blog.getId()%>">

                            <div class="form-group">
                                <textarea class="form-control" name="comment" placeholder="Insert comment" id="comment_area_id"></textarea>
                                <button class="btn btn-success btn-sm mt-3" onclick="addComment()">ADD COMMENT</button>
                            </div>
                    </div>
                </div>
            <script type="text/javascript">
                function addComment() {
                    $.post("/loadcomments", {
                        blog_id: <%=blog.getId()%>,
                        comment: $("#comment_area_id").val()
                    }, function(result) {
                        if(result=="OK") {
                            $("#comment_area_id").val("");
                            loadComments();
                        }
                    });
                }
            </script>
            <%
                }
            %>
            <div class="row mt-3">
                <div class="col-12">
                    <div class="list-group" id="comments_div_id">

                    </div>
                    <script type="text/javascript">
                        function loadComments() {
                            $.get("/loadcomments", {
                                blog_id:<%=blog.getId()%>
                            }, function (json) {
                                var comments=JSON.parse(json);
                                var commentsHTML="";
                                for(var i=0;i<comments.length;i++) {
                                    commentsHTML+="<a href='javaScript:void(0)' class='list-group-item list-group-item-action'>";
                                    commentsHTML+="<div class= 'd-flex w-100 justify-content-between'>";
                                    commentsHTML+="<h5 class='mb-1'>";
                                    commentsHTML+=comments[i].user.fullName;
                                    commentsHTML+="</h5>";
                                    commentsHTML+="<small>"+comments[i].postdate+"</small>";
                                    commentsHTML+="</div>";
                                    commentsHTML+="<p class='mb-1'>"+comments[i].comment+"</p>";
                                    commentsHTML+="</a>";
                                }
                                $("#comments_div_id").html(commentsHTML);
                            });
                        }
                    </script>
                </div>
            </div>
            <%
                } else {
            %>
            <h1 class="text-center">
                404 BLOG NOT FOUND
            </h1>
            <%
                }
            %>
        </div>
    </div>
</div>

</body>
<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</html>
