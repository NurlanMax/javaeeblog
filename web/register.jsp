<%--
  Created by IntelliJ IDEA.
  User: Nurik
  Date: 01/02/2021
  Time: 14:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<%@include file="navbar.jsp"%>
<div class="container">
    <div class="row mt-5">
        <div class="col-6 mx-auto">
            <%
                String error=request.getParameter("error");
                if(error!=null){
            %>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                User with email exists!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <%
                }
            %>

            <%
                String passwordError=request.getParameter("passworderror");
                if(passwordError!=null){
            %>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                Password mismatch!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <%
                }
            %>

            <%
                String success=request.getParameter("success");
                if(success!=null){
            %>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Registration Completed!
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <%
                }
            %>
            <form action="/register"   method="post">
                <div class="form-group">
                    <label>
                        EMAIL:
                    </label>
                    <input type="email" name="email" class="form-control">
                </div>
                <div class="form-group">
                    <label>
                        PASSWORD:
                    </label>
                    <input type="password" name="password" class="form-control">
                </div>
                <div class="form-group">
                    <label>
                       REPEAT PASSWORD:
                    </label>
                    <input type="password" name="re_password" class="form-control">
                </div>
                <div class="form-group">
                    <label>
                        FULL NAME:
                    </label>
                    <input type="text" name="full_name" class="form-control">
                </div>
                <div class="form-group">
                    <button class="btn btn-success">SIGN UP</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</html>
