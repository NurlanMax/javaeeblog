<%@ page import="kz.javaeesession.db.SessionUser" %><%--
  Created by IntelliJ IDEA.
  User: Nurik
  Date: 31/01/2021
  Time: 21:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<%@include file="navbar.jsp"%>
<div class="container">
    <div class="row mt-5">
        <div class="col-6 mx-auto">
            <form action="/addsession" method="post">
                <div class="form-group">
                    <label>NAME : </label>
                    <input type="text" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label>SURNAME : </label>
                    <input type="text" name="surname" class="form-control">
                </div>
                <div class="form-group">
                    <label>AGE : </label>
                    <select name="age" class="form-control">
                        <%
                            for(int i=0;i<=120;i++) {
                        %>
                        <option value="<%=i%>"><%=i%> years</option>
                        <%
                            }
                        %>
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-success">ADD SESSION</button>
                </div>
            </form>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <h1 class="text-center">
                <%
                    SessionUser user=(SessionUser)request.getAttribute("user");
                    String name=(user!=null?user.getName():"No name");
                    String surname=(user!=null?user.getSurname():"No Surname");
                    int age=(user!=null?user.getAge():0);
                %>
                Welcome <%=name%> <%=surname%> <%=age%> years old
            </h1>

        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</html>
