<%@ page import="java.util.ArrayList" %>
<%@ page import="kz.javaeesession.db.Blog" %><%--
  Created by IntelliJ IDEA.
  User: Nurik
  Date: 03/02/2021
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<%@include file="navbar.jsp"%>
<div class="container">
    <div class="row mt-5">
        <div class="col-12">
            <%
                ArrayList<Blog> blogs=(ArrayList<Blog>)request.getAttribute("blogs");
                if(blogs!=null) {
                    for (Blog b:blogs){
            %>
            <div class="jumbotron">
                <h1>
                    <%=b.getTitle()%>
                </h1>
                <p class="lead">
                    <%=b.getShortContent()%>
                </p>
                <hr class="my-4">
                <p>
                    Posted by <b><%=b.getUser().getFullName()%></b> at <b><%=b.getPostDate()%></b>
                </p>
                <a class="btn btn-primary btn-sm" href="/readmore?id=<%=b.getId()%>" role="button">Read More</a>
            </div>
            <%
                }
                }
            %>
        </div>
    </div>
</div>

</body>
<script0 type="text/javascript" src="js/jquery-3.5.1.min.js"></script0>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</html>
